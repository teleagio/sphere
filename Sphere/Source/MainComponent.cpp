/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

//==============================================================================
MainComponent::MainComponent()
{
    // Make sure you set the size of the component after
    // you add any child components.
    setSize (400, 400);

    mCameraAngles[0] = 30.f;
    mCameraAngles[1] = 0.f;
    mCameraAngles[2] = 0.f;

    openGLContext.setRenderer( this );
    openGLContext.setComponentPaintingEnabled( true );
    openGLContext.attachTo( *this );
    openGLContext.setContinuousRepainting( false );
}

MainComponent::~MainComponent()
{
    // This shuts down the GL system and stops the rendering calls.
}

void MainComponent::initLights()
{
    // set up light colors (ambient, diffuse, specular)
//    GLfloat lightKa[] = {.3f, .3f, .3f, 0.1f};  // ambient light
    GLfloat lightKd[] = {.7f, .7f, .7f, 0.1f};  // diffuse light
//    GLfloat lightKs[] = {1, 1, 1, 0.5};           // specular light
//    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
//    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

    // position the light
    float lightPos[4] = {0, 0, 1, 0}; // directional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

    glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
}

//==============================================================================
void MainComponent::newOpenGLContextCreated()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

    glClearColor(0, 0, 0, 0);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);

    initLights();
}

void MainComponent::openGLContextClosing()
{
}

void MainComponent::renderOpenGL()
{
    OpenGLHelpers::clear(getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    float lineColor[] = {0.2f, 0.2f, 0.2f, 1};
    glRotatef(mCameraAngles[0], 1, 0, 0);
    glRotatef(mCameraAngles[1], 0, 1, 0);
    glRotatef(-90, 1, 0, 0);

    mSphere.drawWithLines(lineColor);
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // You can add your component specific drawing code here!
    // This will draw over the top of the openGL background.
}

void MainComponent::resized()
{
    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}
