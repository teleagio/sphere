/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Sphere.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent : public Component, public OpenGLRenderer
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();


    //==============================================================================
    void paint (Graphics& g) override;
    void resized() override;

private:
    OpenGLContext openGLContext;
    Sphere mSphere;
    float mCameraAngles[3];

    void newOpenGLContextCreated() override;
    void renderOpenGL() override;
    void openGLContextClosing() override;
    void initLights();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
